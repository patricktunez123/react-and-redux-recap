import React from 'react';
import ReactDOM from 'react-dom';
import ComentDetails from './ComentDetails';
import ApprovalCard from './ApprovalCard';
import faker from 'faker';
import './App.css';

const App = function () {
  return (
    <div className="ui container comments">
      <ApprovalCard>
        <ComentDetails
          author="Patrick"
          time="Today at 6:00PM"
          comment="This is the comment of Patrick"
          avatar={faker.image.image()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <ComentDetails
          author="Aime"
          time="Today at 2:20PM"
          comment="This is the comment of Aime"
          avatar={faker.image.image()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <ComentDetails
          author="Ange Manzi"
          time="Yesterday at 4:00AM"
          comment="This is the comment of Ange Manzi"
          avatar={faker.image.image()}
        />
      </ApprovalCard>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
